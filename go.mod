module gitlab.com/esr/reposurgeon

require (
	github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/emirpasic/gods v1.12.0
	github.com/google/go-cmp v0.2.0
	github.com/google/uuid v1.1.0
	github.com/ianbruene/go-difflib v1.1.1
	github.com/termie/go-shutil v0.0.0-20140729215957-bcacb06fecae
	gitlab.com/ianbruene/kommandant v0.4.1-0.20190702042627-6ea490d4e5a4
	golang.org/x/crypto v0.0.0-20190219172222-a4c6cb3142f2
	golang.org/x/sys v0.0.0-20190220154126-629670e5acc5 // indirect
	golang.org/x/text v0.3.0
)
